package com.comidas.pedidos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long userid;

    private String fullname;
    private String email;
    private String rolename;
    private String upassword;

    protected User() {}

    public User(String fullname, String email, String rolename, String upassword) {
        this.fullname = fullname;
        this.email = email;
        this.rolename = rolename;
        this.upassword = upassword;
    }

    public Long getUserid() {
        return this.userid;
    }

    public String getFullname() {
        return this.fullname;
    }

    public String getEmail() {
        return this.email;
    }

    public String getUpassword() {
        return this.upassword;
    }

    public String getRolename() {
        return this.rolename;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUpassword(String upassword) {
        this.upassword = upassword;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }
}
 