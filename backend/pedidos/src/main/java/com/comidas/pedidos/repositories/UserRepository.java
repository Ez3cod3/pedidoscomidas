package com.comidas.pedidos;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findByEmailAndUpassword(String email, String upassword);
    User findByUserid(Long userid);
}