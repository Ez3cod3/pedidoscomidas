package com.comidas.pedidos;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestParam;
import com.google.gson.Gson;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("user")
public class UserController {

    private final UserRepository repository;

    UserController(UserRepository repository) {
        this.repository = repository;
    }


    @RequestMapping (
            value = "login/{username}/{upassword}",
            method = RequestMethod.GET,
            produces = {"application/JSON"}
    )
    @ResponseBody
    String login(@PathVariable String username, @PathVariable String upassword) {
        List<User> list = new ArrayList<>();
        this.repository.findByEmailAndUpassword(username, upassword).forEach(e -> list.add(e));

        if (list.size() > 0) {
            return new Gson().toJson(list);
        }else{
            return "{\"result\" : \"failed\"}";
        }
    }

    /*TODO: change this to POST request method*/
    /*@RequestMapping (
        value = "get/{userid}",
        method = RequestMethod.GET,
        produces = {"application/JSON"}
    )
    @ResponseBody
    String getUser(@PathVariable Long userid) {
        User userdata = this.repository.findByUserid(userid);
        return new Gson().toJson(userdata);
    }


    @RequestMapping (value = "signup", method = RequestMethod.POST,
            consumes = {"application/JSON"},
            produces = {"application/JSON"}
    )
    @ResponseBody
    String signup(@RequestBody User newUser){
        this.repository.save(newUser);
        return "{\"result\" : \"created\"}";
    }


    @RequestMapping (value = "update", method = RequestMethod.POST,
            consumes = {"application/JSON"},
            produces = {"application/JSON"}
    )
    @ResponseBody
    String update(@RequestBody User user) {
        User userUpdated = this.repository.findByUserid(user.getUserid());
        userUpdated.setFullname(user.getFullname());
        userUpdated.setEmail(user.getEmail());
        this.repository.save(userUpdated);
        return "{\"result\" : \"updated\"}";
    }


    @RequestMapping (value = "changePassword", method = RequestMethod.POST,
            consumes = {"application/JSON"},
            produces = {"application/JSON"}
    )
    @ResponseBody
    String change(@RequestBody User user) {
        User userUpdated = this.repository.findByUserid(user.getUserid());
        userUpdated.setUpassword(user.getUpassword());
        this.repository.save(userUpdated);
        return "{\"result\" : \"updated\"}";
    }
    */
    @RequestMapping (value = "", method = RequestMethod.GET)
    public String user() {
        /*TODO: Implement this*/
        return "Error 404 : User";
    }
}